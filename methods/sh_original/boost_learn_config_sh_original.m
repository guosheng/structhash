



function train_info=boost_learn_config_sh_old(train_info)


train_info.init_work_info_fn=@init_work_info;
train_info.update_wlearner_info_fn=@update_wlearner_info;
train_info.update_pair_info_cp_fn=@update_pair_info_cp_triplet;
train_info.update_train_result_fn=@update_train_result;


train_info.boost_learn_fn=@boost_learn;

end




function [work_info train_info]=init_work_info(train_info)

train_data=train_info.train_data;

struct_cached_feat=double(train_data.feat_data);


cache_info.cached_feat=struct_cached_feat;



cache_info.relation_info=train_info.relation_info;
cache_info.relation_map=[];


cache_info.e_num=length(cache_info.relation_info.relevant_infos);

cache_info.hfeat=logical([]);

disp('gen_wlearner_cache...');
cache_info=gen_wlearner_cache(train_info, train_data, cache_info);


disp('gen_relation_map duplet...');
gen_param=[];
gen_param.relation_info=cache_info.relation_info;
gen_param.max_relation_num_per_e=inf;
gen_param.gen_type='duplet';
gen_result=gen_relation_map(gen_param);
cache_info.relation_map_duplet=gen_result.relation_map_duplet;
cache_info.relevant_sel_duplet=gen_result.relevant_sel_duplet;
cache_info.relation_inds_es_duplet=gen_result.relation_inds_es_duplet;


% disp('gen_duplet_transfer_cache...');
cache_info=gen_duplet_transfer_cache(cache_info);


disp('gen_relation_map triplet...');
relation_map_es=cell(cache_info.e_num, 1);

for e_idx=1:cache_info.e_num
    relation_gen_param=[];
    relation_gen_param.relation_info=cache_info.relation_info;
    relation_gen_param.max_relation_num_per_e=inf;
    relation_gen_param.gen_type='triplet';

    relation_gen_param.sel_e_idxes=e_idx;
    relation_gen_result=gen_relation_map(relation_gen_param);
    one_full_relation_map=relation_gen_result.relation_map;
    relation_map_es{e_idx}=one_full_relation_map;
end
% 
cache_info.relation_map_es=relation_map_es;



work_info.cache_info=cache_info;
work_info.slack_num=size(struct_cached_feat,1);
work_info.wlearners=[];
work_info.sel_pair_num=0;


train_info.train_data=[];

end


function sel_pair_info=init_sel_pair_info(work_info, cache_info)

    sel_pair_info=[];
    sel_pair_info.pair_label_losses=[];

    sel_pair_info.pair_feat=[];
    sel_pair_info.e_idx_pairs=[];
    sel_pair_info.valid_relation_info_pairs=[];
    sel_pair_info.pair_constants=[];
    sel_pair_info.slack_num=work_info.slack_num;
    sel_pair_info.new_sel_r_num=0;
    sel_pair_info.sel_r_num=0;
    
    sel_pair_info.max_cached_r_num=2^32;
    
    sel_pair_info.wlearner_cache_pairs=[];
    
       
    
end






function work_info=update_wlearner_info(train_info, work_info, wlearner_update_info)


wl_t1_start=tic;


cache_info=work_info.cache_info;
new_wlearner=[];


feat_dim_group_wls=cell(0);
new_wl_idx=work_info.new_up_wl_idx;
next_wl_idx=work_info.next_up_wl_idx;
feat_dim_group_wls{new_wl_idx}=new_wl_idx;

if ~isempty(next_wl_idx)
    feat_dim_group_wls{next_wl_idx}=next_wl_idx;
end

if work_info.solver_init_iter
      
        
    wl_relation_map=cache_info.relation_map_duplet;
    wl_relation_weights=ones(size(wl_relation_map,1),1);
         
    
    sel_pair_info=init_sel_pair_info(work_info, cache_info);
    work_info.infer_t1=0;
    work_info.infer_t2=0;
    work_info.infer_t3=0;
        
    
    cache_info.wl_model=[];
    
    if isfield(train_info, 'init_wlearners')
        new_wlearner=train_info.init_wlearners;
        cache_info.wl_model=train_info.init_wl_model;
                
        feat_dim_group_wls{1}=1:size(new_wlearner, 1);
        new_wl_idx=1:size(new_wlearner, 1);
        
    end
         
    
    
else
    
        
        sel_pair_info=work_info.sel_pair_info;
        valid_relation_info_pairs=sel_pair_info.valid_relation_info_pairs;
        e_idx_pairs=sel_pair_info.e_idx_pairs;
        
        pair_weights=wlearner_update_info.pair_weights;
        pair_idxes=wlearner_update_info.pair_idxes;
        
        wl_relation_map=cache_info.relation_map_duplet;
        wl_relation_weights=zeros(size(wl_relation_map,1),1);
        relation_inds_es_duplet=cache_info.relation_inds_es_duplet;
        

        for p_idx_idx=1:length(pair_idxes)
            
            p_idx=pair_idxes(p_idx_idx);
            one_pair_weight=pair_weights(p_idx_idx);
                                    
            e_idx=e_idx_pairs(p_idx);
            
            one_valid_r_sel=valid_relation_info_pairs{p_idx};
            one_relation_inds_duplet=relation_inds_es_duplet{e_idx};
            
            one_transfer_mat=cache_info.transfer_mat_es{e_idx};
            appear_counts=sum(one_transfer_mat(:,one_valid_r_sel), 2);
            appear_counts=full(appear_counts);
            
            one_weights=wl_relation_weights(one_relation_inds_duplet)+appear_counts.*one_pair_weight;
            wl_relation_weights(one_relation_inds_duplet)=one_weights;            
            
        end
                        
end



wl_t1=toc(wl_t1_start);



t_start=tic;

assert(~isempty(wl_relation_map));
assert(~isempty(wl_relation_weights));


valid_r_num_org=0;
if isempty(new_wlearner)
    
    [new_wlearner, valid_r_num_org cache_info]=gen_wlearner(cache_info, wl_relation_weights, train_info, wl_relation_map);
    
end



wlearner_time=toc(t_start);


wl_t3_start=tic;


if isempty(new_wlearner)
    disp('============== WARNING: new_wlearner is empty!!!! =====================');
    assert(~isempty(work_info.wlearners));
    new_wlearner=work_info.wlearners(end,:);
        
end


new_hfeat=apply_wlearner(cache_info.cached_feat, new_wlearner, cache_info.wl_model);

work_info.new_wlearner=new_wlearner;

work_info.wlearners(new_wl_idx,:)=new_wlearner;

work_info.feat_dim_group_wls=feat_dim_group_wls;

hfeat=cache_info.hfeat;
hfeat(:, new_wl_idx)=new_hfeat;
cache_info.hfeat=hfeat;

work_info.wlearner_dimension=size(hfeat,2);
assert(work_info.wlearner_dimension<=train_info.bit_num);



work_info.wlearner_num=size(work_info.wlearners,1);


new_sel_r_num=sel_pair_info.new_sel_r_num;
sel_r_num=sel_pair_info.sel_r_num;




if ~train_info.cp_hot_start

    work_info.sel_pair_info=[];
    sel_pair_info=init_sel_pair_info(work_info, cache_info);
    
    init_w=ones(work_info.wlearner_num,1).*(1/work_info.wlearner_num);
    if ~work_info.solver_init_iter
        last_w=wlearner_update_info.w;
        init_w(1:length(last_w))=last_w;
        init_w(end)=last_w(end)/10;
    end
    work_info.init_w=init_w;
    work_info.sel_pair_num=0;

elseif ~work_info.solver_init_iter
    
    
    new_pair_feat=[];
    
    ws_pair_idxes=work_info.cp_ws_pair_idxes;
    ws_pair_num=length(ws_pair_idxes);
    
        
    if ws_pair_num>0
        
                
        valid_relation_info_pairs=sel_pair_info.valid_relation_info_pairs;
        if length(ws_pair_idxes)<length(valid_relation_info_pairs)
            valid_relation_info_pairs_tmp=cell(size(valid_relation_info_pairs));
            valid_relation_info_pairs_tmp(ws_pair_idxes)=valid_relation_info_pairs(ws_pair_idxes);
            valid_relation_info_pairs=valid_relation_info_pairs_tmp;
            sel_pair_info.valid_relation_info_pairs=valid_relation_info_pairs;
        end
                
        
        max_pair_idx=max(ws_pair_idxes);
        
      
        new_pair_feat=zeros(max_pair_idx, 1);
                
        e_idx_pairs=sel_pair_info.e_idx_pairs;
        pair_constants=sel_pair_info.pair_constants;
        

        pair_feat_cache_es=cell(cache_info.e_num, 1);
        
        for p_idx_idx=1:ws_pair_num
            p_idx=ws_pair_idxes(p_idx_idx);
                        
            e_idx=e_idx_pairs(p_idx);
            
            one_pair_feat_cache=pair_feat_cache_es{e_idx};
            if isempty(one_pair_feat_cache)
                one_relation_map=cache_info.relation_map_es{e_idx};
                one_pair_feat_cache=do_calc_pair_feat(new_hfeat, one_relation_map);
                pair_feat_cache_es{e_idx}=one_pair_feat_cache;
            end
            
            
            one_valid_r_sel=valid_relation_info_pairs{p_idx};
            one_pair_feat_tmp=one_pair_feat_cache(one_valid_r_sel,:);
            
            new_pair_feat(p_idx,:)=pair_constants(p_idx).*sum(one_pair_feat_tmp,1);
        end
                        
    end
    
    work_info.new_pair_feat_wlearner=new_pair_feat;
    
end

sel_pair_info.new_sel_r_num=0;

work_info.sel_pair_info=sel_pair_info;
work_info.cache_info=cache_info;


wl_t3=toc(wl_t3_start);

sel_pair_num=work_info.sel_pair_num;
fprintf('---wl_info: wl_idx:%d, pair_num:%d, feat_dim:%d, use_kernel_feat:%d\n', ...
    wlearner_update_info.wlearner_iter_idx, sel_pair_num, size(cache_info.cached_feat, 2), train_info.use_wl_kernel);
fprintf('---wl_info: wl_r(Dup):%d, valid_r(Dup):%d, sel_r(Tri):%d, new_sel_r(Tri):%d\n', ...
    size(wl_relation_map,1), valid_r_num_org, sel_r_num, new_sel_r_num);
fprintf('---wl_info: time:(%.1f, %.1f, %.1f), last_infer_ts:(%.1f, %.1f, %.1f), loss:%s \n', ...
    wl_t1, wlearner_time, wl_t3, work_info.infer_t1, work_info.infer_t2, work_info.infer_t3, train_info.label_loss_type);


work_info.infer_t1=0;
work_info.infer_t2=0;
work_info.infer_t3=0;




end








 



function pair_feat=do_calc_pair_feat(hfeat, relation_map, relevant_sel)
       

    if size(relation_map,2)==3
        pair_feat=do_calc_pair_feat_triplet(hfeat, relation_map);
    end

    if size(relation_map,2)==2
        pair_feat=do_calc_pair_feat_duplet(hfeat, relation_map, relevant_sel);
    end
    
end


function pair_feat=do_calc_pair_feat_triplet(hfeat, relation_map)
       

    e_hfeat=hfeat(relation_map(:,1),:);
    en_hfeat=hfeat(relation_map(:,2),:);
    ep_hfeat=hfeat(relation_map(:,3),:);
            
    pair_feat=calc_hfeat_dist(e_hfeat,en_hfeat)-calc_hfeat_dist(e_hfeat,ep_hfeat);

end


function pair_feat=do_calc_pair_feat_duplet(hfeat, relation_map, relevant_sel)
       

    e_hfeat=hfeat(relation_map(:,1),:);
    right_hfeat=hfeat(relation_map(:,2),:);
        
    
    pair_feat=calc_hfeat_dist(e_hfeat,right_hfeat);
    
   
    assert(isnumeric(pair_feat));
    pair_feat(relevant_sel,:)=-pair_feat(relevant_sel,:);

end






function dist=calc_hfeat_dist(d1, d2)



    dist=d1~=d2;
    

    dist=double(dist);


end










function work_info=update_pair_info_cp_triplet(train_info, work_info, pair_update_info)

rescale_type=train_info.rescale_type;
model_w=pair_update_info.model_w;


assert(length(model_w)<=train_info.bit_num);

cache_info=work_info.cache_info;

relation_info=cache_info.relation_info;

sel_pair_info=work_info.sel_pair_info;

e_num=cache_info.e_num;
feat_dim=length(model_w);

valid_new_pair_sel=false(e_num,1);
new_slack_losses=zeros(e_num,1);
new_pair_feat=zeros(e_num,feat_dim);
new_label_losses=zeros(e_num,1);
new_valid_relation_info_pairs=cell(e_num,1);
new_pair_constants=ones(e_num,1);
new_sel_r_num_es=zeros(e_num,1);


t1_start=tic;

relevant_infos=relation_info.relevant_infos;
irrelevant_infos=relation_info.irrelevant_infos;


infer_fn=train_info.infer_fn;


for e_idx=1:e_num

    
    r_num=length(relevant_infos{e_idx})*length(irrelevant_infos{e_idx});
    one_pair_constant=(2/r_num);
    
    infer_info=[];
    infer_info.model_w=model_w;
    infer_info.rescale_type=rescale_type;
    infer_info.e_idx=e_idx;
    infer_info.one_pair_constant=one_pair_constant;
    infer_info.r_num=r_num;

    infer_result=infer_fn(train_info, cache_info, infer_info);
    
    if isempty(infer_result)
        continue;
    end

    one_label_loss=infer_result.one_label_loss;
    one_sel_relation_idxes_sub=infer_result.one_sel_relation_idxes_sub;
    one_new_pair_feat_rank=infer_result.one_new_pair_feat_rank;
    max_loss=infer_result.max_loss;
        

    
            
    if max_loss>0
        
        one_full_relation_map=cache_info.relation_map_es{e_idx};
        
        

        one_valid_r_sel=false(size(one_full_relation_map,1),1);
        one_valid_r_sel(one_sel_relation_idxes_sub)=true;
        
        new_valid_relation_info_pairs{e_idx,1}=one_valid_r_sel;
                       
        
        one_new_pair_feat=sum(one_new_pair_feat_rank,1).*one_pair_constant;
        
        new_pair_feat(e_idx,:)=full(one_new_pair_feat);
        new_slack_losses(e_idx,1)=max_loss;
        new_label_losses(e_idx,1)=one_label_loss;
        new_pair_constants(e_idx)=one_pair_constant;
        valid_new_pair_sel(e_idx)=true;
        new_sel_r_num_es(e_idx)=length(one_sel_relation_idxes_sub);
        
    end
end



t1=toc(t1_start);




t2_start=tic;



new_sel_r_num_es=new_sel_r_num_es(valid_new_pair_sel);
new_sel_r_num=sum(new_sel_r_num_es);



t2=toc(t2_start);





t3_start=tic;


new_pair_feat=new_pair_feat(valid_new_pair_sel,:);
new_slack_losses=new_slack_losses(valid_new_pair_sel,:);
new_label_losses=new_label_losses(valid_new_pair_sel,:);
new_pair_constants=new_pair_constants(valid_new_pair_sel,:);
new_valid_relation_info_pairs=new_valid_relation_info_pairs(valid_new_pair_sel,:);
new_e_idx_pairs=find(valid_new_pair_sel);

pair_idx_counter=length(sel_pair_info.valid_relation_info_pairs);
new_pir_num=length(new_slack_losses);
new_pair_idxes=(pair_idx_counter+1:pair_idx_counter+new_pir_num)';


assert(length(new_e_idx_pairs)==length(new_valid_relation_info_pairs));


sel_pair_info.valid_relation_info_pairs=cat(1, sel_pair_info.valid_relation_info_pairs, new_valid_relation_info_pairs);
sel_pair_info.pair_constants=cat(1, sel_pair_info.pair_constants, new_pair_constants);
sel_pair_info.e_idx_pairs=cat(1, sel_pair_info.e_idx_pairs, new_e_idx_pairs);

sel_pair_info.new_sel_r_num=sel_pair_info.new_sel_r_num+new_sel_r_num;
sel_pair_info.sel_r_num=sel_pair_info.sel_r_num+new_sel_r_num;


if ~isempty(new_pair_idxes)
    assert(length(sel_pair_info.valid_relation_info_pairs)==max(new_pair_idxes));
end


work_info.sel_pair_info=sel_pair_info;


new_pair_info.pair_label_losses=new_label_losses;
new_pair_info.pair_feat=new_pair_feat;
new_pair_info.slack_losses=new_slack_losses;
new_pair_info.pair_idxes=new_pair_idxes;


work_info.sel_pair_num=work_info.sel_pair_num+length(new_pair_idxes);
work_info.new_pair_info=new_pair_info;

work_info.cache_info=cache_info;

t3=toc(t3_start);

work_info.infer_t1=work_info.infer_t1+t1;
work_info.infer_t2=work_info.infer_t2+t2;
work_info.infer_t3=work_info.infer_t3+t3;



end








function cache_info=gen_duplet_transfer_cache(cache_info)

disp('gen_duplet_transfer_cache...');
            
            relation_map_duplet=cache_info.relation_map_duplet;
            relation_inds_es_duplet=cache_info.relation_inds_es_duplet;
            relevant_sel_duplet=cache_info.relevant_sel_duplet;
            e_num=length(relation_inds_es_duplet);
            
                    
            
            transfer_mat_es=cell(e_num,1);
       
fprintf('processing examples (%d):\n', e_num);

        for e_idx=1:e_num
            
                
                if mod(e_idx, 50)==0
                    fprintf('%d.', e_idx);
                end
                
                if mod(e_idx, 1000)==0
                    fprintf('\n');
                end
                
                relation_gen_param=[];
                relation_gen_param.relation_info=cache_info.relation_info;
                relation_gen_param.max_relation_num_per_e=inf;
                relation_gen_param.gen_type='triplet';
            
                relation_gen_param.sel_e_idxes=e_idx;
                relation_gen_result=gen_relation_map(relation_gen_param);
                one_relation_map=relation_gen_result.relation_map;
                                
                                            
                one_r_inds_duplet=relation_inds_es_duplet{e_idx};
                one_relation_map_duplet=relation_map_duplet(one_r_inds_duplet,:);
                one_relevant_sel_duplet=relevant_sel_duplet(one_r_inds_duplet,:);
                                                
               
                one_irrelevant_sel_duplet=~one_relevant_sel_duplet;
                                    
                neg_mat=bsxfun(@eq, one_relation_map_duplet(one_irrelevant_sel_duplet,2), one_relation_map(:,2)');
                pos_mat=bsxfun(@eq, one_relation_map_duplet(one_relevant_sel_duplet,2), one_relation_map(:,3)');
    
               
                one_transfer_mat=false(size(one_relation_map_duplet,1), size(one_relation_map,1));
                one_transfer_mat(one_irrelevant_sel_duplet,:)=neg_mat;
                one_transfer_mat(one_relevant_sel_duplet,:)=pos_mat;
                
                transfer_mat_es{e_idx}=sparse(one_transfer_mat);
        end
        
        fprintf('\n');
      
        cache_info.transfer_mat_es=transfer_mat_es;
         
end







function train_result=update_train_result(train_result, work_info, train_info)

% not necessary now...
% train_result.model.hs=clean_wlearners(train_result.model.hs);

end



function one_label_loss=calc_label_loss_PrecAtK(one_rank, pos_idxes, test_k)

assert(size(one_rank,2)==1);
Agree=ismember(one_rank, pos_idxes);

PK=mean( Agree(1:test_k, :));

one_label_loss=1-PK;

end


function one_label_loss=calc_label_loss_AUC(one_rank, pos_idxes, test_k)

assert(size(one_rank,2)==1);
Agree=ismember(one_rank, pos_idxes);

Agree=Agree(1:max_knn_auc,:);
TPR             = cumsum(Agree,     1);
FPR             = cumsum(~Agree,    1);

numPos          = TPR(end,:);
numNeg          = FPR(end,:);

numPos=max(numPos,1);
numNeg=max(numNeg,1);
        
        %AUC measure expect all relevant example place before irreleveant
        %examples, the Agree matric indicate the ground true relevance.
   
FPR_tmp=FPR;
FPR_tmp(~Agree)=0;
aucs_tmp=1-(sum(FPR_tmp,1)./(numPos.*numNeg));
AUC=aucs_tmp;
        

one_label_loss=1-AUC;

end





function one_label_loss=calc_label_loss_NDCG(one_rank, pos_idxes, test_k)

assert(size(one_rank,2)==1);
Agree=ismember(one_rank, pos_idxes);

nTrain=length(one_rank);
Discount        = zeros(1, nTrain);
Discount(1:2)   = 1;
NDCG   = -Inf;
for k = test_k
    Discount(3:k)   = 1 ./ log2(3:k);
    Discount        = Discount / sum(Discount);

    b = mean(Discount * Agree);
    if b > NDCG
        NDCG = b;
    end
end
   
one_label_loss=1-NDCG;

end










