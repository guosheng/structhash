




function train_result_sub=boost_solver_cp_stage(train_info, work_info)



ts_all=tic;


C=train_info.tradeoff_C;
org_slack_num=work_info.slack_num;



%norm_type: 2: for l2 norm, structens, 1 for l1 norm, 3 for l2norm with w>=0 constraints
norm_type=1;
% norm_type=2;
% norm_type=3;
if isfield(train_info,'norm_type')
    norm_type=train_info.norm_type;
end



epsilon=1e-1;
if isfield(train_info,'cp_epsilon')
    epsilon=train_info.cp_epsilon;
end

min_epsilon=1e-6;
if isfield(train_info,'cp_epsilon_min')
    min_epsilon=train_info.cp_epsilon_min;
end
min_epsilon=min(min_epsilon, epsilon);


cp_force_obj_decrease=true;
if isfield(train_info,'cp_force_obj_decrease')
    cp_force_obj_decrease=train_info.cp_force_obj_decrease;
end






cp_hot_start=true;
if isfield(train_info,'cp_hot_start')
    cp_hot_start=train_info.cp_hot_start;
end





max_iteration_sub=1e6;
min_iteration_sub=0;


if isfield(train_info,'min_iteration_sub')
    min_iteration_sub=train_info.min_iteration_sub;
end
if isfield(train_info,'max_iteration_sub')
    max_iteration_sub=train_info.max_iteration_sub;
end


max_ws_clean_ratio=1;

ws_clean_age_thresh=200;

do_ws_clean=true;


ws_clean_dual_thresh_max=inf;
ws_clean_dual_thresh_min=1e-12;


ws_clean_dual_thresh_mul_max=1e-8;



if norm_type==2 || norm_type==3
    ws_clean_dual_thresh_max=inf;
end





if isfield(train_info,'do_ws_clean')
    do_ws_clean=train_info.do_ws_clean;
end

if isfield(train_info,'max_ws_clean_ratio')
    max_ws_clean_ratio=train_info.max_ws_clean_ratio;
end

if isfield(train_info,'ws_clean_age_thresh')
    ws_clean_age_thresh=train_info.ws_clean_age_thresh;
end

if isfield(train_info,'ws_clean_dual_thresh_max')
    ws_clean_dual_thresh_max=train_info.ws_clean_dual_thresh_max;
end

if isfield(train_info,'ws_clean_dual_thresh_mul_max')
    ws_clean_dual_thresh_mul_max=train_info.ws_clean_dual_thresh_mul_max;
end


if max_ws_clean_ratio==0
    do_ws_clean=false;
end




solver_quite=true;
if isfield(train_info,'solver_quite')
    solver_quite=train_info.solver_quite;
end

% use matlab qp solver
% qp_solver_type=4;

% use  qp solver
qp_solver_type=3;

if isfield(train_info,'qp_solver_type')
    qp_solver_type=train_info.qp_solver_type;
end


max_ws_size_hot_start=inf;
if isfield(train_info,'max_ws_size_hot_start')
    max_ws_size_hot_start=train_info.max_ws_size_hot_start;
end





workset=[];
workset_ages=[];
pair_feat_cp=[];
pair_label_losses_ws=[];
margin_vs_cp=[];




%simplex
mosek_solver_type_norm1=1;
%interior point
% mosek_solver_type_norm1=2;


% currently only support this:
assert(mosek_solver_type_norm1==1);

solver_info=[];
solver_info.mosek_solver_type_norm1=mosek_solver_type_norm1;
solver_info.C=C;
solver_info.org_slack_num=org_slack_num;
solver_info.norm_type=norm_type;
solver_info.quite=solver_quite;
solver_info.qp_solver_type=qp_solver_type;
solver_info.max_ws_size_hot_start=max_ws_size_hot_start;
solver_info.max_ws_clean_ratio=max_ws_clean_ratio;
solver_info.ws_clean_age_thresh=ws_clean_age_thresh;
solver_info.min_epsilon=min_epsilon;

solver_info.use_l2_loss=false;
if norm_type==2 && qp_solver_type==1
    solver_info.use_l2_loss=true;
end



solver_info.force_ksi_positive=true;


rescale_type=train_info.rescale_type;



recent_new_dual_vs=inf;



if norm_type==1
    call_solver_fn=@call_solver_norm1;
    do_solver_init_fn=@do_solver_init_norm1;
    calc_obj_fv_fn=@calc_obj_fv_norm1;
    clean_workset_fn=@clean_workset_norm1;
end




w_dim=work_info.wlearner_dimension;


init_w=(w_dim:-1:1)./norm(w_dim:-1:1).*0.1.*rand(1,w_dim);
init_w=sort(init_w,'descend');
init_w=init_w';

if isfield(work_info,'init_w') && ~isempty(work_info.init_w)
    init_w=work_info.init_w;
end
assert(length(init_w)==w_dim);
model_w=init_w;



last_objFv=inf;
last_objFv_cp=inf;
last_chk_cp_eps=epsilon;


last_train_cache=[];
if isfield(work_info, 'train_cache_sub')
    last_train_cache=work_info.train_cache_sub;
    last_objFv=last_train_cache.objFv;
    last_objFv_cp=last_train_cache.objFv_cp;
    last_chk_cp_eps=last_train_cache.chk_cp_eps;
end

if ~cp_hot_start
    last_train_cache=[];
end



solver_info.do_warm_start_init=false;
solver_info.last_solver_cache=[];

remove_ws_num=0;


one_ws_clean_dual_thresh_mul=ws_clean_dual_thresh_mul_max;
one_ws_clean_dual_thresh=ws_clean_dual_thresh_min;



cp_iter_nums=[];
mean_ws_sizes=[];
one_ws_sizes=[];


if isempty(last_train_cache)
    
    new_w_dim=w_dim;   
    solver_w_dim=w_dim;
   
else
   

    cp_iter_nums=last_train_cache.cp_iter_nums;
    mean_ws_sizes=last_train_cache.mean_ws_sizes;
    

    solver_cache=last_train_cache.solver_cache;
    
    workset=last_train_cache.workset;
    workset_ages=last_train_cache.workset_ages;
    pair_label_losses_ws=last_train_cache.pair_label_losses_ws;
    margin_vs_cp=last_train_cache.margin_vs_cp;
    pair_feat_cp=last_train_cache.pair_feat_cp;
    
    
    last_w_dim=size(pair_feat_cp, 2);          
    new_w_dim=w_dim-last_w_dim;
    
    
    pair_feat_cp=sum(pair_feat_cp,2);


    model_w=ones(1+new_w_dim,1);
    model_w(end-new_w_dim+1:end)=0;
        
       
        
        
    solver_info.last_solver_cache=solver_cache;
    if new_w_dim>0
        solver_info.do_warm_start_init=true;
    end
        
    work_info.workset_pair_idxes=workset;
    new_pair_feat_hs_cp=new_pair_feat_wlearner_fn(workset, work_info, rescale_type, pair_label_losses_ws, org_slack_num);
    pair_feat_cp=cat(2, pair_feat_cp, new_pair_feat_hs_cp);
   
    solver_w_dim=size(pair_feat_cp, 2);
    
end

solver_info.new_w_dim=new_w_dim;
solver_info.w_dim=solver_w_dim;
solver_info=do_solver_init_fn(solver_info);


cp_iter_num=0;
finished=false;

slack_loss_cp=0;
infer_t=0;
opt_t=0;


chk_cp_eps=inf;
chk_obj_gap=inf;
objFv=inf;


no_new_pair=false;

solver_info.chk_cp_eps=last_chk_cp_eps;
solver_info.cp_epsilon=epsilon;


assert(do_ws_clean);


cp_eps_using=epsilon;



fprintf('\n---solver_cp, inference iters:');



if ~isempty(workset)
    
    solver_info.no_new_pair=true;
    solver_info.pair_feat_cp=pair_feat_cp;
    solver_info.margin_vs_cp=margin_vs_cp;

    solver_result=call_solver_fn(solver_info);
    model_w=solver_result.model_w;
            
    slack_loss_cp=solver_result.slack_loss_cp;
end



solver_result=[];
solver_result.model_w=model_w;
solver_result.slack_loss_cp=slack_loss_cp;
solver_result.solver_eps=0;



avg_recent_cp_iter=0;
cp_iter_recent_num=10;
if ~isempty(cp_iter_nums)
    sel_cp_idxes=(length(cp_iter_nums)-cp_iter_recent_num+1:length(cp_iter_nums));
    sel_cp_idxes=sel_cp_idxes(sel_cp_idxes>0);
    avg_recent_cp_iter=mean(cp_iter_nums(sel_cp_idxes));
end




while ~finished

                
        ts=tic;
        
        cp_iter_num=cp_iter_num+1;
        
        eva_cp_iter_num=avg_recent_cp_iter/2+cp_iter_num;
        

        infer_model_w=model_w;
        if w_dim>2
            infer_model_w=zeros(w_dim, 1);
            assert(length(model_w)==2);
            infer_model_w(1:w_dim-1)=model_w(1);
            infer_model_w(end)=model_w(2);
        end
        
        pair_update_info=[];
        pair_update_info.model_w=infer_model_w;
        

        pair_update_info.sub_iter_idx=cp_iter_num;
        pair_update_info.converge_value=chk_cp_eps;
        pair_update_info.slack_loss_cp=slack_loss_cp;
        
        work_info=train_info.update_pair_info_cp_fn(train_info, work_info, pair_update_info);
        
               
        infer_t=infer_t+toc(ts);

        new_pair_info=work_info.new_pair_info;
        new_pair_label_losses=new_pair_info.pair_label_losses;
        new_pair_feat=new_pair_info.pair_feat;
        new_pair_idxes=new_pair_info.pair_idxes;
        new_slack_losses=new_pair_info.slack_losses;
        
   
        if isempty(new_pair_idxes)
            no_new_pair=true;
        end

        if cp_iter_num==1
%             assert(~no_new_pair);
        end

       

                
        if cp_iter_num>0
            
       
            
            [objFv objFv_cp chk_cp_eps]=calc_obj_fv_fn(new_pair_info, solver_result, solver_info);
            chk_obj_gap=objFv-objFv_cp;
            
                                           


                solver_info.chk_cp_eps=chk_cp_eps;
                
                if chk_cp_eps<-1e-9
                    fprintf('\n\n######## WARNING: cutting plane chk_cp_eps should >0 :%.4f, maybe inference problem!!!!\n\n', chk_cp_eps);
                end
                
                              
              
                if chk_cp_eps<=epsilon 
                    finished=true;
                    finish_type=1;
                end


                obj_small_v=eps;
                not_objfv_decrease=(last_objFv+obj_small_v)<objFv;
                not_objfv_cp_increase=(objFv_cp+obj_small_v)<last_objFv_cp;
                                    
                if finished && cp_force_obj_decrease
                
                                
                    
                    if  not_objfv_decrease && chk_cp_eps > last_chk_cp_eps
                        
                        cancel_finish=false;
                        
                        cp_eps_using=last_chk_cp_eps;

                                                
                        if cp_iter_num==1
                            cancel_finish=true;
                        end
 
                        if chk_cp_eps>min_epsilon 
                            cancel_finish=true;
                        else
                            cp_eps_using=min_epsilon;
                        end
                                               
                        
                        if cancel_finish
                            finished=false;
                            finish_type=0;
                        end

                    end
                    
                end
                
                
            if mod(cp_iter_num, 100)==0 || finished
                fprintf('%d.', cp_iter_num);
            end
            
            if mod(cp_iter_num, 1000)==0 || finished

                    fprintf('\n---solver_cp, eps_chk:%.4f(%.4f), solver_eps:%.1d, avg_cp_iter(%d):%.1f \n', ... 
                        chk_cp_eps, cp_eps_using, solver_result.solver_eps, cp_iter_recent_num, avg_recent_cp_iter);
                                            
                    fprintf('---solver_cp, ws_clean_mul:%.1d, ws_clean:%.1d, obj_cp(up:%d):%.5f, obj(down:%d):%.5f\n', ...
                    	one_ws_clean_dual_thresh_mul, one_ws_clean_dual_thresh,...
                        ~not_objfv_cp_increase, objFv_cp, ~not_objfv_decrease,  objFv);

                                    
                if ~finished
                    fprintf('---solver_cp, inference iters: ');
                else
%                     fprintf('\n');
                end
            end
            
       
            
        end

        if cp_iter_num<=min_iteration_sub
            finished=false;
            finish_type=0;
        end
        
        if cp_iter_num>=max_iteration_sub
            finished=true;
            finish_type=2;
            
            
                fprintf('######## WARNING: exceeed max_iteration_sub:%d \n', max_iteration_sub);
            
        end
        
        
        
        if no_new_pair
       
            finished=true;
            finish_type=3;
            break;
        end
                
        
        
        if ~no_new_pair
            
            assert(min(new_slack_losses)>0);
            

            if rescale_type==1
                new_pair_feat=bsxfun(@times, new_pair_feat, new_pair_label_losses );
            end

            new_ws_num=length(workset);
            
                        
            new_pair_feat_cp=sum(new_pair_feat,1)./org_slack_num;
            

            if size(new_pair_feat_cp, 2)>2
                tmp_new_pair_feat_cp=sum(new_pair_feat_cp(:, 1:end-1));
                new_pair_feat_cp=cat(2, tmp_new_pair_feat_cp, new_pair_feat_cp(:, end));
            end
            
            pair_feat_cp=cat(1, pair_feat_cp, new_pair_feat_cp);
            new_margin_vs_cp=sum(new_pair_label_losses,1)./org_slack_num;
            margin_vs_cp=cat(1, margin_vs_cp, new_margin_vs_cp);

            workset{end+1}=new_pair_idxes;
            workset_ages(end+1,1)=0;
            pair_label_losses_ws{end+1}=new_pair_label_losses;
            one_ws_sizes(end+1)=length(workset);
            

                   
            
            new_ws_num=length(workset)-new_ws_num;
            
            
            assert(~isempty(pair_feat_cp));

            solver_info.no_new_pair=no_new_pair;
            solver_info.pair_feat_cp=pair_feat_cp;
            solver_info.margin_vs_cp=margin_vs_cp;

            solver_result=call_solver_fn(solver_info);
            model_w=solver_result.model_w;
            slack_loss_cp=solver_result.slack_loss_cp;
                                    
            
            dual_sol=solver_result.dual_sol;
            solver_cache=solver_result.solver_cache;
            
                       
            if new_ws_num>1
                one_new_dual_v=min(dual_sol(end-new_ws_num+1:end));
            else
                one_new_dual_v=dual_sol(end);
            end
            
            recent_new_dual_vs=min(recent_new_dual_vs, one_new_dual_v);
            
            
            if do_ws_clean
                
                                
                
                        
                if ~isempty(workset_ages)
                    

                    one_ws_clean_dual_thresh_mul=(0.1^(eva_cp_iter_num/100));
                                          
                    one_ws_clean_dual_thresh_mul=min(one_ws_clean_dual_thresh_mul, ws_clean_dual_thresh_mul_max);
                    
                   
                    ws_num=length(workset_ages);
                    tmp_dual_v=min(min(recent_new_dual_vs), 0.1*C/ws_num);

                    one_ws_clean_dual_thresh=one_ws_clean_dual_thresh_mul*tmp_dual_v;
                    
                    
                    one_ws_clean_dual_thresh=min(one_ws_clean_dual_thresh, ws_clean_dual_thresh_max);
                    one_ws_clean_dual_thresh=max(one_ws_clean_dual_thresh, ws_clean_dual_thresh_min);
                    
                    
                    zero_sel=dual_sol<one_ws_clean_dual_thresh;
                    one_age=1;
                                        
                    if eva_cp_iter_num>1000
                        one_age=0.5;
                    end
                    workset_ages(zero_sel)=workset_ages(zero_sel)+one_age;
                    nnz_sel=~zero_sel;
                    workset_ages(nnz_sel)=0;
                end

                solver_info.workset_ages=workset_ages;
                [sel_idxes solver_cache]=clean_workset_fn(solver_cache, solver_info);

                one_remove_ws_num=length(workset)-length(sel_idxes);

                if one_remove_ws_num>0
                    workset=workset(sel_idxes);
                    workset_ages=workset_ages(sel_idxes);
                    pair_label_losses_ws=pair_label_losses_ws(sel_idxes);
                    margin_vs_cp=margin_vs_cp(sel_idxes);
                    pair_feat_cp=pair_feat_cp(sel_idxes,:);
                    dual_sol=dual_sol(sel_idxes,:);
                    
                    remove_ws_num=remove_ws_num+one_remove_ws_num;
                end
                
                solver_result.dual_sol=dual_sol;
                solver_result.solver_cache=solver_cache;

            end
            
                        
            solver_info.last_solver_cache=solver_cache;
            
            opt_t=opt_t+solver_result.opt_time;

        end
    
        
end



fprintf('\ncurrent solution of model_w:\n');
disp(model_w);


model_w=ones(w_dim, 1);

pair_update_info=[];
pair_update_info.model_w=model_w;


objFv_gap=chk_obj_gap;

if objFv_gap < -1e-8
    fprintf(' \n #### WARNNING: objFv_gap = objFv_org - objFv_cp < 0: %.4f\n\n', objFv_gap);
end



mean_ws_sizes(end+1)=mean(one_ws_sizes);
cp_iter_nums(end+1)=cp_iter_num;

solver_cache=solver_info.last_solver_cache;

train_cache.pair_feat_cp=pair_feat_cp;

train_cache.solver_cache=solver_cache;
train_cache.w=model_w;
train_cache.workset=workset;
train_cache.workset_ages=workset_ages;
train_cache.pair_label_losses_ws=pair_label_losses_ws;
train_cache.margin_vs_cp=margin_vs_cp;
train_cache.objFv=objFv;
train_cache.objFv_cp=objFv_cp;
train_cache.cp_iter_nums=cp_iter_nums;
train_cache.mean_ws_sizes=mean_ws_sizes;
train_cache.chk_cp_eps=chk_cp_eps;
train_cache.recent_new_dual_vs=recent_new_dual_vs;




max_pair_idx=0;
if ~isempty(workset)
    max_pari_idx_wss=cellfun(@max, workset);
    max_pair_idx=max(max_pari_idx_wss);
end


ps_lambda=zeros(max_pair_idx,1);
ws_pair_idxes=[];

if cp_hot_start
    
    for onesl_con_idx=1:length(workset)
           one_sel_pair_idxes=workset{onesl_con_idx};
           ws_pair_idxes=cat(1, ws_pair_idxes, one_sel_pair_idxes(:));
    end
    
    ws_pair_idxes=unique(ws_pair_idxes);
end

if ~no_new_pair
    
    oneslack_lambda=dual_sol;
    assert(length(workset)==length(oneslack_lambda))
    oneslack_lambda=oneslack_lambda./org_slack_num;


    if rescale_type==1
        for onesl_con_idx=1:length(workset)
            onesl_lambda_m=oneslack_lambda(onesl_con_idx);
            one_sel_pair_idxes=workset{onesl_con_idx};
            one_label_losses=pair_label_losses_ws{onesl_con_idx};
            ps_lambda(one_sel_pair_idxes)=ps_lambda(one_sel_pair_idxes)+onesl_lambda_m.*one_label_losses;
        end
    end


    if rescale_type==2
        for onesl_con_idx=1:length(workset)
            onesl_lambda_m=oneslack_lambda(onesl_con_idx);
            one_sel_pair_idxes=workset{onesl_con_idx};
            ps_lambda(one_sel_pair_idxes)=ps_lambda(one_sel_pair_idxes)+onesl_lambda_m;
        end
    end

    ws_pair_idxes=unique(ws_pair_idxes);

end




most_cg_vio=inf;

work_info.train_cache_sub=train_cache;

train_result_sub.method='cp';
train_result_sub.w=model_w;
train_result_sub.iter_num=cp_iter_num;
train_result_sub.objectiveF_v=objFv;

sel_pair_idxes=find(ps_lambda);



train_result_sub.most_cg_vio=most_cg_vio;
train_result_sub.wlearner_pair_weight=ps_lambda(sel_pair_idxes);
train_result_sub.wlearner_pair_idxes=sel_pair_idxes;


work_info.cp_ws_pair_idxes=ws_pair_idxes;

train_result_sub.work_info=work_info;

all_t=toc(ts_all);


fprintf('---solver_cp, cp_iter:%d, norm_type:%d, all_t:%.1f, infer_t:%.1f, opt_t:%.1f\n',...
          cp_iter_num, norm_type, all_t, infer_t, opt_t);
fprintf('---solver_cp, ws_size:%d, remove_ws:%d, age_thresh:%d, avg_infer_num:%d, avg_ws_size:%d\n',...
    length(workset), remove_ws_num, ws_clean_age_thresh, round(mean(cp_iter_nums)), round(mean(mean_ws_sizes)));
    
 

end




function new_pair_feat_hs_cp=new_pair_feat_wlearner_fn(workset, work_info, rescale_type, pair_label_losses_ws, slack_num)

new_pair_feat_hs=work_info.new_pair_feat_wlearner;

if isempty(new_pair_feat_hs)
   new_pair_feat_hs_cp=[]; 
   assert(isempty(workset));
   return;
end

assert(~isempty(workset));

new_pair_feat_hs_cp=zeros(length(workset), size(new_pair_feat_hs,2));

max_pair_idx=0;

for w_idx=1:length(workset)
    
    one_pair_idxes=workset{w_idx};
       
    max_pair_idx=max(max_pair_idx, max(one_pair_idxes));
        

    one_new_pair_feat_hs_cp=new_pair_feat_hs(one_pair_idxes,:);
    
    if rescale_type==1
        one_new_pair_feat_hs_cp=bsxfun(@times, one_new_pair_feat_hs_cp, pair_label_losses_ws{w_idx});
    end
    
    one_new_pair_feat_hs_cp=sum(one_new_pair_feat_hs_cp,1)./slack_num;
    new_pair_feat_hs_cp(w_idx,:)=one_new_pair_feat_hs_cp;
end


try
assert(size(new_pair_feat_hs,1)>=max_pair_idx);
catch
    dbstack
    keyboard
end


end








%=========================================================================================================




function solver_result=call_solver_norm1(solver_info)

pair_feat_cp=solver_info.pair_feat_cp;
mosek_solver_type_norm1=solver_info.mosek_solver_type_norm1;
margin_vs_cp=solver_info.margin_vs_cp;
no_new_pair=solver_info.no_new_pair;

last_res=[];
last_solver_cache=solver_info.last_solver_cache;
if ~isempty(last_solver_cache)
    last_res=last_solver_cache.last_res;
end

c=solver_info.c;
blx=solver_info.blx;

slack_dim=1;

bux=[];
buc=[];

a_ksi=ones(size(pair_feat_cp,1), 1);
a=cat(2,a_ksi, pair_feat_cp);


blc=margin_vs_cp;


param=[];
param.MSK_IPAR_LOG=0;
    
   

    svm_solver_eps=gen_solver_eps_lp(solver_info);
    
    last_prob=[];
    if mosek_solver_type_norm1==1

        % using simplex solver
             

        param.MSK_IPAR_OPTIMIZER='MSK_OPTIMIZER_PRIMAL_DUAL_SIMPLEX';
                              

        
        param.MSK_IPAR_PRESOLVE_USE=0;

        
        if svm_solver_eps>0
            param.MSK_DPAR_BASIS_TOL_S=svm_solver_eps;
            param.MSK_DPAR_BASIS_TOL_X=svm_solver_eps;
        end
        
      
        
    end
    
           

    cmd  = 'minimize echo(0)';

    
    prob=last_prob;
    prob.c=c;

    prob.a=a;
    
    prob.blc=blc;
    prob.buc=buc;
    prob.blx=blx;
    prob.bux=bux;
    
    ts=tic;
    [rcode,res]  = mosekopt(cmd,prob,param);
    opt_t=toc(ts);
    
    if ~isfield(res, 'sol')
        disp('mosek solver error!!!');
        disp(res);
        keyboard;
        error('mosek error');
    end
    
    
    if mosek_solver_type_norm1==1
        last_res=res;
        sol_obj=res.sol.bas;
    end

    if mosek_solver_type_norm1==2
        sol_obj=res.sol.itr;
    end

    
    x_primal = sol_obj.xx;
    model_w=x_primal(slack_dim+1:end);
    slack_loss_cp=x_primal(1);
    
dual_sol=sol_obj.slc;




solver_cache=[];
solver_cache.last_res=last_res;
solver_cache.dual_sol=dual_sol;
    
solver_result=[];
solver_result.model_w=model_w;
solver_result.slack_loss_cp=slack_loss_cp;
solver_result.dual_sol=dual_sol;
solver_result.opt_time=opt_t;
solver_result.solver_cache=solver_cache;
solver_result.solver_eps=svm_solver_eps;

    

end





function solver_info=do_solver_init_norm1(solver_info)

C=solver_info.C;
w_dim=solver_info.w_dim;
new_w_dim=solver_info.new_w_dim;


slack_dim=1;


c_w=ones(w_dim,1);
c_ksi=ones(slack_dim,1)*C;
c=cat(1,c_ksi,c_w);


blx_w=zeros(w_dim,1);


if solver_info.force_ksi_positive
    blx_ksi=zeros(slack_dim,1);
else
    blx_ksi=-inf(slack_dim,1);
end


blx=cat(1,blx_ksi, blx_w);

solver_info.blx=blx;
solver_info.c=c;
    

mosek_solver_type_norm1=solver_info.mosek_solver_type_norm1;

if mosek_solver_type_norm1==1

    if solver_info.do_warm_start_init
        
       
       last_solver_cache=solver_info.last_solver_cache;
       last_res=last_solver_cache.last_res;


        bas          = last_res.sol.bas;
        
        new_v_zeros=zeros(new_w_dim,1);
        new_v_skx=repmat('LL', new_w_dim,1);
        


            bas.skx      = [bas.skx;new_v_skx];


            bas.xx       = [bas.xx;new_v_zeros];
            bas.slx      = [bas.slx;new_v_zeros];
            bas.sux      = [bas.sux;new_v_zeros];
            
        last_res.sol.bas = bas;
                
        last_solver_cache.last_res=last_res;
        solver_info.last_solver_cache=last_solver_cache;
    end
    
    
    
end
      



end






function [objFv objFv_cp chk_cp_eps]=calc_obj_fv_norm1(new_pair_info, solver_result, solver_info)

    new_slack_losses=new_pair_info.slack_losses;
    sum_new_slack_losses=0;
    if ~isempty(new_slack_losses)
        sum_new_slack_losses=sum(new_slack_losses);
    end
            
    org_slack_num=solver_info.org_slack_num;
    C=solver_info.C;
    model_w=solver_result.model_w;
    slack_loss_cp=solver_result.slack_loss_cp;
    
    objFv=sum(model_w)+(C/org_slack_num)*sum_new_slack_losses;
    objFv_cp=sum(model_w)+C*slack_loss_cp;
    chk_cp_eps=sum_new_slack_losses/org_slack_num - slack_loss_cp;
    
end




function [sel_idxes solver_cache ws_sel]=clean_workset_norm2(solver_cache, solver_info)


workset_ages=solver_info.workset_ages;
age_thresh=solver_info.ws_clean_age_thresh;

ws_sel=workset_ages<age_thresh;
ws_sel_idxes=find(ws_sel);
sel_idxes=ws_sel_idxes;



max_remove_ratio=solver_info.max_ws_clean_ratio;
if max_remove_ratio<1
    
    error('not support!!!');
    
    dual_sol=solver_cache.dual_sol;
    
    ws_num=length(dual_sol);
    ws_clean_thresh=solver_info.ws_clean_thresh;
    zero_thresh=ws_clean_thresh;
        
    max_ws_size_hot_start=solver_info.max_ws_size_hot_start;
    
    if length(ws_sel_idxes)<ws_num
        tmp_dual_sol=dual_sol;
        tmp_dual_sol(ws_sel_idxes)=inf;

        [sort_dual_sol sort_idxes]=sort(tmp_dual_sol);
        sel_pos=find(sort_dual_sol>zero_thresh,1);

        sel_pos=min(sel_pos, ceil(length(sort_idxes)*max_remove_ratio));


        tmp_sel_idxes=sel_pos:ws_num;
        if length(tmp_sel_idxes)>max_ws_size_hot_start
            tmp_sel_idxes=tmp_sel_idxes(end-max_ws_size_hot_start+1, end);
        end
        sel_idxes=sort_idxes(tmp_sel_idxes);

        ws_sel=false(ws_num,1);
        ws_sel(sel_idxes)=true;

    else

        ws_sel=true(ws_num,1);

    end


    solver_cache.dual_sol=dual_sol(ws_sel);
    sel_idxes=find(ws_sel);

end


end



function [sel_idxes solver_cache]=clean_workset_norm1(solver_cache, solver_info)

[sel_idxes solver_cache ws_sel]=clean_workset_norm2(solver_cache, solver_info);

last_res=solver_cache.last_res;

if ~isempty(last_res)
    bas = last_res.sol.bas;

    bas.skc      = bas.skc(ws_sel,:);
    bas.xc       = bas.xc(ws_sel,:);
    bas.y        = bas.y(ws_sel,:);
    bas.slc      = bas.slc(ws_sel,:);
    bas.suc      = bas.suc(ws_sel,:);

    last_res.sol.bas=bas;
    solver_cache.last_res=last_res;
end

end





%===========================================================================================================================================================



















function svm_solver_eps=gen_solver_eps_lp(solver_info)
% 
    min_cp_eps=min(solver_info.chk_cp_eps, solver_info.cp_epsilon);
    min_cp_eps=max(min_cp_eps, solver_info.min_epsilon);

    svm_solver_eps=1e-2*min_cp_eps;
    
    svm_solver_eps=min(1e-1, svm_solver_eps);
    
    if svm_solver_eps<=1e-6
        svm_solver_eps=-1;
    end
    

    
end





































