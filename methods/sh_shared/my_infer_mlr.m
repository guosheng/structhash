

function infer_result=my_infer_mlr(train_info, cache_info, infer_info)


infer_fn=train_info.do_infer_fn_mlr;

relation_info=cache_info.relation_info;
e_num=cache_info.e_num;



hfeat=cache_info.hfeat;
model_w=infer_info.model_w;
rescale_type=infer_info.rescale_type;
e_idx=infer_info.e_idx;
one_pair_constant=infer_info.one_pair_constant;


one_full_relation_map=cache_info.relation_map_es{e_idx};


relevant_infos=relation_info.relevant_infos;
irrelevant_infos=relation_info.irrelevant_infos;



one_new_pair_feat_rank=[];
max_loss=[];



    label_loss_type=train_info.label_loss_type;
    precision_at_k=train_info.precision_at_k;


        
        pos_e_idxes=relevant_infos{e_idx};
        neg_e_idxes=irrelevant_infos{e_idx};
                   
        if length(pos_e_idxes)<3
           
            
            infer_result=[];
            return;
            
        end
                
        ep_hfeat=hfeat(pos_e_idxes,:);
        en_hfeat=hfeat(neg_e_idxes,:);
        
        pos_num=length(pos_e_idxes);
        neg_num=length(neg_e_idxes);
        e_hfeat_tmp=hfeat(e_idx,:);
        e_hfeat=repmat(e_hfeat_tmp, pos_num,1);
        pos_dist=calc_hfeat_dist(e_hfeat,ep_hfeat)*model_w;
        e_hfeat=repmat(e_hfeat_tmp, neg_num,1);
        neg_dist=calc_hfeat_dist(e_hfeat,en_hfeat)*model_w;
        
        nn_num=pos_num+neg_num;
        
        pos_idxes_tmp=1:pos_num;
        neg_idxes_tmp=pos_num+1:nn_num;
        
        
      
        pos_dist=pos_dist+eps;
        
        D=zeros(nn_num,1);
        D(pos_idxes_tmp,1)=pos_dist;
        D(neg_idxes_tmp,1)=neg_dist;
        
        
        
        one_precision_at_k=min(pos_num, precision_at_k);
        
        [one_rank_tmp, one_label_loss] = infer_fn(1, D, pos_idxes_tmp, neg_idxes_tmp, one_precision_at_k);
        
       
        if strcmp(label_loss_type, 'AUC')
            one_label_loss=2*(1-one_label_loss);
        end
                
        [~, tmp_rank_vs]=sort(one_rank_tmp);
        
        rank_vs=zeros(e_num,1);
        rank_vs(cat(1, pos_e_idxes, neg_e_idxes))=tmp_rank_vs;
        
        
        tmp_sel=rank_vs(one_full_relation_map(:,2))<rank_vs(one_full_relation_map(:,3));
        one_sel_relation_idxes_sub=find(tmp_sel);
        one_relation_map=one_full_relation_map(tmp_sel,:);
        one_r_num=length(one_sel_relation_idxes_sub);
               
        
        if one_r_num>0
            
       
            
            one_new_pair_feat_rank=do_calc_pair_feat(hfeat, one_relation_map);
            sel_scores_org=one_new_pair_feat_rank*model_w;
            sel_scores_org_sum=sum(sel_scores_org).*one_pair_constant;    
            
            assert(one_label_loss>0);
            
            if rescale_type==1
                error('not support!');
            end

            if rescale_type==2
                max_loss=one_label_loss-sel_scores_org_sum;
            end
                
        else
               
        
                        
            relevant_indicator=false(1, pos_num+neg_num);
            relevant_indicator(1:pos_num)=true;
            relevant_indicator=relevant_indicator(one_rank_tmp);
            
            try
                assert(all(relevant_indicator(1:pos_num)));
            catch
                dbstack;
                keyboard;
            end
            
           
            
            max_loss=0;
            if one_label_loss>1e-5
                fprintf('\n\n ######################## WARNING, correct list, but one_label_loss:%.3f\n\n', one_label_loss);      
                dbstack;
                keyboard;
            end
            
        end
        
        
        
        infer_result=[];
            infer_result.one_label_loss=one_label_loss;
            infer_result.one_sel_relation_idxes_sub=one_sel_relation_idxes_sub;
            infer_result.one_new_pair_feat_rank=one_new_pair_feat_rank;
            infer_result.max_loss=max_loss;
        
end


