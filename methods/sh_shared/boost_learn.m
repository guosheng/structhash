

function train_result=boost_learn(train_info)




fprintf('\n\n############### training, train_id:%s #################\n\n',train_info.train_id);



if false
    
    % necessary params:
    
    if ~isfield(train_info,'solver_type')
        train_info.solver_type='lbfgs';
    end

    if ~isfield(train_info,'max_iteration_num')
        train_info.max_iteration_num=1000;
    end
    
end


if ~isfield(train_info,'valid_wl_num')
    train_info.valid_wl_num=train_info.max_iteration_num;
end

if ~isfield(train_info,'max_run_time')
    train_info.max_run_time=inf; %inf hours
end


if ~isfield(train_info,'fix_iter_num')
    train_info.fix_iter_num=true;
end

if ~isfield(train_info,'cg_epsilon')
    train_info.cg_epsilon=1e-5;
end


if ~isfield(train_info,'solver_fn')
    train_info.solver_fn=[];
else
    train_info.solver_type='custom';
end


if ~isfield(train_info,'notes');
    train_info.notes='';
end


if ~isfield(train_info,'rescale_type')
    % margin resacle as default
    train_info.rescale_type=2;
end


if ~isfield(train_info,'update_train_result_fn')
    train_info.update_train_result_fn=[];
end



if ~isfield(train_info,'diff_epsilon')
    train_info.diff_epsilon=-inf;
end

disp('train_info:');
disp(train_info);
   
    
    
    fprintf('------- init_work_info_fn ...\n');
    [train_info_ext.work_info train_info]=train_info.init_work_info_fn(train_info);
    [train_result train_result_ext]=do_train(train_info, train_info_ext);
    



update_train_result_fn=train_info.update_train_result_fn;
if ~isempty(update_train_result_fn)
    train_result=update_train_result_fn(train_result, train_result_ext.work_info, train_info);
end





end




%========================================================================================================================================================


function [train_result train_result_ext]=do_train(train_info, train_info_ext)

train_id=train_info.train_id;
cg_epsilon=train_info.cg_epsilon;
diff_epsilon=train_info.diff_epsilon;


max_run_time=train_info.max_run_time;
max_iteration_num=train_info.max_iteration_num;


train_cache.wlearners=[];
train_cache.wl_model=[];
train_cache.objectiveF_v_iters=[];
train_cache.wlearner_time_iters=[];
train_cache.method_time_iters=[];
train_cache.method_time_nocg_iters=[];
train_cache.wlearner_iter_idx=[];


wlearner_iter_idx=0;
cached_method_time=0;


train_finished=false;
method_tstart=tic;
cg_converge=false;
most_cg_vio=inf;
method_time=0;
opt_time_iter=0;


chk_diff_k=20;
avg_objFv_diff=inf;


fprintf('\n\n-------------------------------------------------------------------------\n\n');

objFv_iters=[];

wlearner_update_info=[];


work_info=train_info_ext.work_info;
work_info.solver_init_iter=true;

valid_wl_num=train_info.valid_wl_num;
valid_wl_idx=0;


while ~train_finished

%-----------------------------check converge------------------------------

    if wlearner_iter_idx>=max_iteration_num 
        fprintf(' \n ---------------------- reach max iteration, %d >= %d \n',wlearner_iter_idx,max_iteration_num);
        train_finished=true;
    end
                
    if method_time + opt_time_iter > max_run_time 
        fprintf(' \n ---------------------- next iteration will reach max run_time %.1f + %.1f > %.1f\n', method_time, opt_time_iter, max_run_time);
        train_finished=true;
    end

    if ~train_info.fix_iter_num

         if most_cg_vio<= cg_epsilon
             fprintf(' \n\n --------- cg_vio converge, train finished! most_cg_vio:%.10f, cg_epsilon:%.10f  \n\n',most_cg_vio, cg_epsilon);
         end


       if wlearner_iter_idx>chk_diff_k
            sel_iter_idxes=length(objFv_iters)-chk_diff_k+1:length(objFv_iters)-1;
            sel_iter_idxes2=sel_iter_idxes+1;
            avg_objFv_diff=sum(abs(objFv_iters(sel_iter_idxes)-objFv_iters(sel_iter_idxes2)));
            if avg_objFv_diff<diff_epsilon
                fprintf(' \n\n --------- objective no significant changes, train finished! avg_objFv_diff:%.10f, diff_epsilon:%.10f, chk_diff_k:%d  \n\n',...
                    avg_objFv_diff, diff_epsilon, chk_diff_k);
                cg_converge=true;
                train_finished=true;
            end
       end

    end      

    
    
    train_cache.train_finished=train_finished;
    train_cache.cg_converge=cg_converge;   
    
    if train_finished
        break;
    end
    
%-----------------------------check converge end ------------------------------


    wlearner_iter_idx=wlearner_iter_idx+1;
    wlearner_update_info.wlearner_iter_idx=wlearner_iter_idx;        
    
    one_wlearner_ts=tic;
    
    valid_wl_idx=valid_wl_idx+1;
    new_up_wl_idx=valid_wl_idx;
    next_up_wl_idx=valid_wl_idx+1;
        
    if valid_wl_num<=valid_wl_idx
        valid_wl_idx=0;
        new_up_wl_idx=valid_wl_num;
        next_up_wl_idx=1;
    end
    if wlearner_iter_idx>=max_iteration_num 
        next_up_wl_idx=[];
    end
    work_info.new_up_wl_idx=new_up_wl_idx;
    work_info.next_up_wl_idx=next_up_wl_idx;
    
    pre_up_wl_idx=new_up_wl_idx-1;
    if pre_up_wl_idx==0
        pre_up_wl_idx=valid_wl_num;
    end
    work_info.pre_up_wl_idx=pre_up_wl_idx;
    
    
     work_info=train_info.update_wlearner_info_fn(train_info, work_info, wlearner_update_info);
    
            
    wlearner_time_iter=toc(one_wlearner_ts);
    
    ts_one_cg_train=tic;
    train_result_sub=do_train_sub(train_info, work_info);
    opt_time_iter=toc(ts_one_cg_train);
               
    work_info=train_result_sub.work_info;
    

    wlearner_num=work_info.wlearner_num;
    pair_num=work_info.sel_pair_num;
    
    
    if isnan(train_result_sub.objectiveF_v)
        error('objective is NAN');
    end
   
        
    cur_w=full(train_result_sub.w);
    wlearner_update_info.pair_weights=train_result_sub.wlearner_pair_weight;
    wlearner_update_info.pair_idxes=train_result_sub.wlearner_pair_idxes;
    wlearner_update_info.w=cur_w;
            
    
      
    
    cur_objectiveF_v=train_result_sub.objectiveF_v;
    most_cg_vio=train_result_sub.most_cg_vio;
    sub_iter_num=train_result_sub.iter_num;
        
    objFv_iters(end+1)=cur_objectiveF_v;
           
    
    if isempty(next_up_wl_idx)
       next_up_wl_idx=0;
    end
    fprintf(['---boost_learn: trn_id:%s, wl_iter:%d/%d, wl_up:(%d/%d, next:%d) wl_num:%d\n'],...
        train_id, wlearner_iter_idx, max_iteration_num, new_up_wl_idx, valid_wl_num, next_up_wl_idx, wlearner_num);           
    fprintf(['---boost_learn: m_t:%.1f, wl_t:%.1f, opt_t:%.1f, solver_type:%s, obj:%.6f \n'],...
        method_time, wlearner_time_iter, opt_time_iter, train_info.solver_type, cur_objectiveF_v);
    

    train_cache.w=cur_w;
    train_cache.w_iters{wlearner_iter_idx}=cur_w;
    train_cache.wlearners=work_info.wlearners;
    train_cache.cur_objFv=cur_objectiveF_v;
    train_cache.objFv_iters(wlearner_iter_idx)=cur_objectiveF_v;
    
    train_cache.wlearner_iter_idx=wlearner_iter_idx;
        
    train_cache.wlearner_time_iters(wlearner_iter_idx)=wlearner_time_iter;
    train_cache.opt_time_iters(wlearner_iter_idx)=opt_time_iter;
    method_time=toc(method_tstart)+cached_method_time;
    train_cache.method_time_iters(wlearner_iter_idx)=method_time;
    
    train_cache.pair_num=pair_num;
    train_cache.wlearner_num=wlearner_num;
    
        
    if isfield(work_info, 'wl_model')
        train_cache.wl_model=work_info.wl_model;
    end
    
        
    work_info.solver_init_iter=false;
	    
end

fprintf('\n\n-------------------------------------------------------------------------\n\n');
if cg_converge
    fprintf('\n++++++++++ cg iteration converged! train_id:%s, cg_vio:%.4f +++++++++++\n',train_id, most_cg_vio);
end

fprintf('\n\n================================= training finished! train_id:%s, cg_iter num:%d \n\n',...
    train_id, wlearner_iter_idx);


train_result=gen_train_result(train_info, train_cache);


train_result_ext.work_info=work_info;


end



function model=gen_model(train_info, train_cache)

if ~isfield(train_info,'ext_model_info')
    train_info.ext_model_info=[];
end

model.notes=train_info.notes;
model.name=train_info.notes;
model.ext_model_info=train_info.ext_model_info;
model.train_id=train_info.train_id;


model.w=train_cache.w;
model.hs=train_cache.wlearners;
model.wl_model=train_cache.wl_model;

end




function train_result=gen_train_result(train_info, train_cache)



train_result=train_cache;
train_result.wlearners=[];


train_result.model=gen_model(train_info, train_cache);


wlearner_iter_idx=train_cache.wlearner_iter_idx;

train_result.wlearner_iter_num=wlearner_iter_idx;

train_result.obj_Fv=train_cache.cur_objFv;
train_result.method_time=train_cache.method_time_iters(end);
train_result.wlearner_time=sum(train_cache.wlearner_time_iters);
train_result.opt_time=sum(train_cache.opt_time_iters);
train_result.method_time_nocg=train_result.method_time-train_result.wlearner_time;

train_result.tradeoff_C=train_info.tradeoff_C;
train_result.rescale_type=train_info.rescale_type;

train_result.cons_num=train_cache.pair_num;

train_result.train_time=train_result.method_time;
train_result.iter_num=train_result.wlearner_iter_num;
train_result.is_converged=false;
train_result.train_id=train_info.train_id;

end







function train_result_sub=do_train_sub(train_info, work_info)

    if isempty(train_info.solver_fn)

        switch train_info.solver_type

        	case 'cutting-plane-original'

                train_result_sub=boost_solver_cp_original(train_info, work_info);                

            case 'cutting-plane-stage'

                train_result_sub=boost_solver_cp_stage(train_info, work_info);                

            otherwise

                error('not supported solver');
        end
    
    else
        train_result_sub=train_info.solver_fn(train_info, work_info);
    end
 

end





