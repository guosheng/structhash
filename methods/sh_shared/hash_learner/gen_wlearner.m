

function [wlearner, valid_r_num, cache_info] =gen_wlearner(cache_info, relation_weights, train_info, relation_map)


t0=tic;

assert(~issparse(relation_weights));
assert(~issparse(relation_map));

assert(isa(relation_map,'uint32'));

 

zero_thresh=1e-10;
r_sel=relation_weights>zero_thresh;
relation_weights(~r_sel)=0;
valid_r_inds=find(r_sel);
valid_r_num=length(valid_r_inds);


if valid_r_num>0
    relation_weights=valid_r_num*(relation_weights./norm(relation_weights,1));
else
    wlearner=[];
    fprintf('\n\n=========================== WARNING: wl_valid_e_num is 0 ===================================\n\n');
    return;
end



max_valid_wlearner_r_num=train_info.max_valid_wlearner_r_num;
if valid_r_num>max_valid_wlearner_r_num
            
    sort_idxes=randsample(valid_r_num, max_valid_wlearner_r_num);
    
    valid_r_inds=valid_r_inds(sort_idxes(1:max_valid_wlearner_r_num));
end

valid_r_num=length(valid_r_inds);


is_duplet=size(relation_map,2)==2;
assert(is_duplet);


    
relevant_sel_duplet=cache_info.relevant_sel_duplet;

        
    
wlearner_score=-inf;
wlearner=[];
wl_notes=[];

fprintf('\n');
    
        
    
if train_info.do_wl_train_spectral

    tmp_t=tic;

    [one_wlearner] =gen_wl_spectral(cache_info, relation_weights, ...
        relation_map, train_info, valid_r_inds, relevant_sel_duplet);
    
    one_wlearner_score=calc_wl_score(cache_info.cached_feat, relation_map, ...
        relation_weights, relevant_sel_duplet, one_wlearner);

    one_wl_notes='spectral';
    if one_wlearner_score>wlearner_score;
        wlearner=one_wlearner;
        wlearner_score=one_wlearner_score;
        wl_notes=one_wl_notes;
    end

    one_wl_t=toc(tmp_t);
    fprintf('---wl_training, type:%s, time:%.1f, score:%.4f\n', one_wl_notes, one_wl_t, one_wlearner_score);

end





if train_info.do_wl_train_random

    tmp_t=tic;

    [one_wlearner] =gen_wlearner_random_duplet(cache_info, relation_weights,...
        relation_map, train_info, valid_r_inds, relevant_sel_duplet);
    
    one_wlearner_score=calc_wl_score(cache_info.cached_feat, relation_map, ...
        relation_weights, relevant_sel_duplet, one_wlearner);

    one_wl_notes='random';
    if one_wlearner_score>wlearner_score;
        wlearner=one_wlearner;
        wlearner_score=one_wlearner_score;
        wl_notes=one_wl_notes;
    end 

    one_wl_t=toc(tmp_t);
    fprintf('---wl_training, type:%s, time:%.1f, score:%.4f\n', one_wl_notes, one_wl_t, one_wlearner_score);

end
       

   
if train_info.do_wl_train_lbfgs
    
    tmp_t=tic;
    
    init_wlearner=wlearner;
    one_wlearner =gen_wlearner_lbfgs(cache_info, relation_weights, relation_map, ...
        train_info, valid_r_inds, relevant_sel_duplet, init_wlearner);
        
    
    one_wlearner_score=calc_wl_score(cache_info.cached_feat, relation_map, ...
        relation_weights, relevant_sel_duplet, one_wlearner);
          
    one_wl_notes='lbfgs';
    if one_wlearner_score>wlearner_score;
        wlearner=one_wlearner;
        wlearner_score=one_wlearner_score;
        wl_notes=one_wl_notes;
    end 

    one_wl_t=toc(tmp_t);
    fprintf('---wl_training, type:%s, time:%.1f, score:%.4f\n', one_wl_notes, one_wl_t, one_wlearner_score);
    
end

       


cache_info.wl_gen_time=toc(t0);

    
end












function [wlearner] =gen_wlearner_random_duplet(cache_info, relation_weights, relation_map, train_info, valid_r_inds, relevant_sel)


    can_num=train_info.wlearner_random_sample_num;

    feat_data=cache_info.cached_feat;
    assert(~issparse(feat_data));
    
    dim_num=size(feat_data,2);
    sample_mean=zeros(can_num, dim_num);
    
    w_cans=normrnd(sample_mean, 1);
    b_cans=rand(can_num,1);
    w_cans_ext=cat(2, w_cans,b_cans);
    
    wlearner_scores=calc_wl_score_wlearners(feat_data,...
        relation_map, relation_weights, relevant_sel, w_cans_ext);
    
        
    [~, sel_idx]=max(wlearner_scores);
    wlearner=w_cans_ext(sel_idx,:);
 

end












function [wlearner] =gen_wlearner_lbfgs(cache_info, relation_weights, relation_map, train_info, valid_r_inds, relevant_sel_duplet, init_wlearner)



assert(~isempty(init_wlearner));

relation_map=relation_map(valid_r_inds,:);
relation_weights=relation_weights(valid_r_inds,:);
relevant_sel_duplet=relevant_sel_duplet(valid_r_inds,:);
relation_weights(relevant_sel_duplet)=-relation_weights(relevant_sel_duplet);

aux_data=cell(0);
aux_data{1}=relation_map;

aux_data{2}=relation_weights;

aux_data{3}=cache_info.cached_feat;

approx_fator=1;
aux_data{4}=approx_fator;


wlearner_reg_v=0;
wlearner_reg_v=length(valid_r_inds)*wlearner_reg_v;
aux_data{7}=wlearner_reg_v;


max_lbfgs_iter=train_info.max_lbfgs_iter;


[wlearner wl_fail]=do_gen_wlearner_random_lbfgs(aux_data, init_wlearner, max_lbfgs_iter);


if isempty(wlearner)
     wlearner=init_wlearner;
end

end



function [wlearner wl_fail]=do_gen_wlearner_random_lbfgs(aux_data, init_wlearner, maxiter)



objFv_func_name='gen_wlearner_lbfgsb_calc_objFv';
grad_func_name='gen_wlearner_lbfgsb_calc_grad';

lb_epsilon=1e-4;

wlearner=init_wlearner;

w_dim=length(init_wlearner);
lb=-inf(w_dim,1);
ub=inf(w_dim,1);

assert(size(init_wlearner, 1)==1);

wl_fail=false;
try
    wlearner = lbfgsb(wlearner, lb, ub, objFv_func_name, grad_func_name, aux_data, [], 'factr', lb_epsilon, 'maxiter', maxiter);
catch e
    
    disp(e);
    
    keyboard;

    fprintf('\n\n ################### WARNING: lbfgs solver failed, probably bad initialization. ###################\n\n');
    wl_fail=true;
end

end







function [wlearner] =gen_wl_spectral(cache_info, relation_weights, ...
    relation_map, train_info, valid_r_inds, relevant_sel_duplet)

relation_map=relation_map(valid_r_inds,:);
relation_weights=relation_weights(valid_r_inds,:);
relevant_sel_duplet=relevant_sel_duplet(valid_r_inds,:);


e_num=size(cache_info.cached_feat, 1);
S=zeros(e_num, e_num);
tmp_inds=sub2ind(size(S), relation_map(:, 1), relation_map(:, 2));
tmp_S=-relation_weights;
tmp_S(relevant_sel_duplet)=-tmp_S(relevant_sel_duplet);
S(tmp_inds)=tmp_S;
S=S+S';



trn=e_num;
feat_data = cache_info.cached_feat;


KK = feat_data;



RM=cache_info.RM;


   
LM = KK'*S*KK;



LM=LM+LM';
[U, V]=eigs(LM, RM, 1, 'la', struct([]));


wlearner = U(:,1);
tep = wlearner'*RM*wlearner;
wlearner = sqrt(trn/tep)*wlearner;
wlearner=wlearner';
     

wlearner=cat(2, wlearner, 0);
        
    
end







function wlearner_score=calc_wl_score(feat_data, relation_map, relation_weights, relevant_sel_duplet, wlearner)


assert(size(wlearner, 1)==1);

if size(wlearner, 2)==size(feat_data, 2)+1

    hfeat=feat_data*wlearner(1:end-1)';
    hfeat=hfeat+wlearner(end);
else
    error('should not come here!');
    hfeat=feat_data*wlearner';
end

hfeat=hfeat>0;

pair_dist=hfeat(relation_map(:,1))~=hfeat(relation_map(:,2));
scores=pair_dist.*relation_weights;
scores(relevant_sel_duplet)=-scores(relevant_sel_duplet);
wlearner_score=sum(scores);


end






function wlearner_scores=calc_wl_score_wlearners(feat_data,...
    relation_map, relation_weights, relevant_sel_duplet, wlearners)


if size(wlearners, 2)==size(feat_data, 2)+1
    hfeat=feat_data*wlearners(:, 1:end-1)';
    hfeat=hfeat+repmat(wlearners(:, end)', size(feat_data, 1), 1);
else
    error('should not come here!');
    hfeat=feat_data*wlearner';
end

hfeat=hfeat>0;

relation_weights(relevant_sel_duplet)=-relation_weights(relevant_sel_duplet);
r1=relation_map(:,1);
r2=relation_map(:,2);

wlearner_scores=zeros(size(wlearners,1), 1);

for w_idx=1:size(wlearners,1)
    one_hfeat=hfeat(:,w_idx);
    pair_dist=one_hfeat(r1)~=one_hfeat(r2);
    one_score=sum(relation_weights(pair_dist));
    wlearner_scores(w_idx)=one_score;
end


end




