
function infer_result=my_infer_auc(train_info, cache_info, infer_info)

    hfeat=cache_info.hfeat;
    model_w=infer_info.model_w;
    rescale_type=infer_info.rescale_type;
    e_idx=infer_info.e_idx;
    one_pair_constant=infer_info.one_pair_constant;
    r_num=infer_info.r_num;

        one_full_relation_map=cache_info.relation_map_es{e_idx};
        
        if isempty(one_full_relation_map)
            relation_gen_param=[];
            relation_gen_param.relation_info=relation_info;
            relation_gen_param.max_relation_num_per_e=inf;
            relation_gen_param.gen_type='triplet';

            relation_gen_param.sel_e_idxes=e_idx;
            relation_gen_result=gen_relation_map(relation_gen_param);
            one_full_relation_map=relation_gen_result.relation_map;
            cache_info.relation_map_es{e_idx}=one_full_relation_map;
        end

        
        
    
        %TODO calc score first
        one_relation_feat=do_calc_pair_feat(hfeat, one_full_relation_map);
        sel_scores_org=one_relation_feat*model_w;
        


        [sel_scores_org_sort, sort_idxes]=sort(sel_scores_org);
        sel_scores_org_sort_cumsum=cumsum(sel_scores_org_sort).*one_pair_constant;


        one_label_loss_cans=cumsum(repmat(1/(r_num), r_num,1));

        loss_cans=[];
        if rescale_type==1
            loss_cans=one_label_loss_cans - one_label_loss_cans.*sel_scores_org_sort_cumsum;
        end

        if rescale_type==2
            loss_cans=one_label_loss_cans-sel_scores_org_sort_cumsum;
        end


        
        
        
        one_label_loss=[];
        one_sel_relation_idxes_sub=[];
        one_new_pair_feat_rank=[];
        
        [max_loss, max_idx]=max(loss_cans);
        
        if max_loss>0
            one_label_loss=one_label_loss_cans(max_idx);
            one_sel_relation_idxes_sub=sort_idxes(1:max_idx);
            one_new_pair_feat_rank=one_relation_feat(one_sel_relation_idxes_sub,:);
        end

        
        infer_result=[];
        infer_result.one_label_loss=one_label_loss;
        infer_result.one_sel_relation_idxes_sub=one_sel_relation_idxes_sub;
        infer_result.one_new_pair_feat_rank=one_new_pair_feat_rank;
        infer_result.max_loss=max_loss;
end

